package com.example.esservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author Qiurz
 * @date 2019/11/25 16:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ElasticEntity<T> {

    /**
     *  主键标识
     */
    private String id;

    /**
     *  JSON对象，实际存储数据
     */
    private Map data;
}
