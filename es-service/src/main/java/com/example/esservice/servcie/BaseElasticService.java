package com.example.esservice.servcie;

import com.alibaba.fastjson.JSON;
import com.example.esservice.entity.ElasticEntity;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Qiurz
 * @date 2019/11/25 14:18
 */
@Slf4j
@Component
public class BaseElasticService {

    @Autowired
    RestHighLevelClient restHighLevelClient;

    /**
     *  创建索引
     * @param idxName
     * @param idxSQL
     */
    public void createIndex(String idxName,String idxSQL) throws Exception{
        //判断索引是否存在
        if (!this.indexExist(idxName)) {
            log.info("idxName:[{}]已存在！idxSQL:[{}]",idxName,idxSQL);
        }
        CreateIndexRequest request = new CreateIndexRequest(idxName);
        //设置分片
        request.settings(Settings.builder().put("index.number_of_shards",3)
                .put("index.number_of_replicas",2));
        //设置mapping字段
        request.mapping(idxSQL, XContentType.JSON);

        CreateIndexResponse response = restHighLevelClient.indices().create(request,RequestOptions.DEFAULT);
        if (!response.isAcknowledged()) {
            log.error("初始化失败！");
            throw new RuntimeException("初始化失败！");
        }

    }

    /**
     *  判断索引是否存在
     * @param idxName
     * @return
     */
    private boolean indexExist(String idxName) throws Exception{
        GetIndexRequest request = new GetIndexRequest(idxName);
        request.local(false);
        request.humanReadable(true);
        request.includeDefaults(false);
        request.indicesOptions(IndicesOptions.lenientExpandOpen());
        return restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
    }

    /**
     *  判断索引是否存在
     * @param idxName
     * @return
     */
    public boolean isExistsIndex(String idxName) throws Exception{
        return restHighLevelClient.indices().exists(new GetIndexRequest(idxName),RequestOptions.DEFAULT);
    }


    /**
     *  更新保存
     * @param idxName
     * @param entity
     */
    public void insertOrUpdate(String idxName, ElasticEntity entity) {
        IndexRequest request = new IndexRequest(idxName);
        request.id(entity.getId());
        request.source(entity.getData(),XContentType.JSON);

        try {
            restHighLevelClient.index(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     *  删除
     * @param idxName
     * @param entity
     */
    public void delete(String idxName, ElasticEntity entity) {
        DeleteRequest request = new DeleteRequest(idxName);
        request.id(entity.getId());
        try {
            restHighLevelClient.delete(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     *  批量插入
     * @param idxName
     * @param lists
     */
    public void insertBatch(String idxName, List<ElasticEntity> lists) {
        BulkRequest request = new BulkRequest(idxName);

        lists.forEach(item -> request.add(new IndexRequest(idxName).id(item.getId())
                .source(item.getData(), XContentType.JSON)));

        try {
            restHighLevelClient.bulk(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *  批量删除
     * @param idxName
     * @param ids
     */
    public void deleteBatch(String idxName, List<String> ids) {

        try {
            BulkRequest request = new BulkRequest(idxName);

            ids.forEach(item -> request.add(new DeleteRequest(idxName,item)));
            restHighLevelClient.bulk(request,RequestOptions.DEFAULT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    /**
     *  查询
     * @param idxName
     * @param builder
     * @param c
     * @param <T>
     * @return
     */
    public <T> List<T> search(String idxName, SearchSourceBuilder builder,Class<T> c) {
        List<T> searchResult = new ArrayList<>();
        SearchRequest request = new SearchRequest(idxName);
        request.source(builder);

        try {
            SearchResponse response = restHighLevelClient.search(request,RequestOptions.DEFAULT);
            SearchHit[] hits = response.getHits().getHits();
            if (hits != null && hits.length > 0) {
                for (SearchHit hit:hits) {
                    searchResult.add(JSON.parseObject(hit.getSourceAsString(),c));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return searchResult;
    }

}
