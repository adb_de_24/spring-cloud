package com.example.esservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author Qiurz
 * @date 2019/11/25 15:03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdxVo {

    private String idxName;

    private IdxSql  idxSql ;


    public static class IdxSql{
        private Map<String, Map<String, Object>> properties;

        public Map<String, Map<String, Object>> getProperties() {
            return properties;
        }

        public void setProperties(Map<String, Map<String, Object>> properties) {
            this.properties = properties;
        }
    }

}
