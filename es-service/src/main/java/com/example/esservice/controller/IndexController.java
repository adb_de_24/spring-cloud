package com.example.esservice.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.esservice.entity.ElasticEntity;
import com.example.esservice.model.ElasticDataVo;
import com.example.esservice.model.IdxVo;
import com.example.esservice.model.QueryVo;
import com.example.esservice.servcie.BaseElasticService;
import com.example.esservice.util.ElasticUtil;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Qiurz
 * @date 2019/11/25 15:01
 */
@Slf4j
@RestController
public class IndexController {

    @Autowired
    BaseElasticService baseElasticService;

    @RequestMapping(value = "/createIndex",method = RequestMethod.POST)
    public Map<String,Object> createIndex(@RequestBody IdxVo idxVo){
        Map<String,Object> response = new HashMap<>();
        try {
            //索引不存在，再创建，否则不允许创建
            if(!baseElasticService.isExistsIndex(idxVo.getIdxName())){
                 String idxSql = JSONObject.toJSONString(idxVo.getIdxSql());
                log.warn(" idxName={}, idxSql={}",idxVo.getIdxName(),idxSql);
                baseElasticService.createIndex(idxVo.getIdxName(),idxSql);
                response.put("code",10000);
                response.put("msg","成功创建");
            } else{
                response.put("code",20000);
                response.put("msg","索引已经存在，不允许创建");
            }
        } catch (Exception e) {
            response.put("code",99999);
            response.put("msg",e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public Map<String,Object> add(@RequestBody ElasticDataVo elasticDataVo){
        Map<String,Object> response = new HashMap<>();
        try {
            if(StringUtils.isEmpty(elasticDataVo.getIdxName())){
                response.put("code",20000);
                response.put("msg","索引为空，不允许提交");
                log.warn("索引为空");
                return response;
            }
            ElasticEntity elasticEntity = new ElasticEntity();
            elasticEntity.setId(elasticDataVo.getElasticEntity().getId());
            elasticEntity.setData(elasticDataVo.getElasticEntity().getData());

            baseElasticService.insertOrUpdate(elasticDataVo.getIdxName(), elasticEntity);

        } catch (Exception e) {
            response.put("code",20000);
            response.put("msg","服务忙，请稍后再试");
            log.error("插入数据异常，metadataVo={},异常信息={}", elasticDataVo.toString(),e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/get",method = RequestMethod.POST)
    public Map<String,Object> get(@RequestBody QueryVo queryVo){

        Map<String,Object> response = new HashMap<>();

        if(StringUtils.isEmpty(queryVo.getIdxName())){
            response.put("code",20000);
            response.put("msg","索引为空，不允许提交");
            log.warn("索引为空");
            return response;
        }

        try {
            Class<?> clazz = ElasticUtil.getClazz(queryVo.getClassName());
            Map<String,Object> params = queryVo.getQuery().get("match");
            Set<String> keys = params.keySet();
            MatchQueryBuilder queryBuilders=null;
            for(String ke:keys){
                queryBuilders = QueryBuilders.matchQuery(ke, params.get(ke));
            }
            if(null!=queryBuilders){
                SearchSourceBuilder searchSourceBuilder = ElasticUtil.initSearchSourceBuilder(queryBuilders);
                List<?> data = baseElasticService.search(queryVo.getIdxName(),searchSourceBuilder,clazz);
                response.put("data",data);
                response.put("code",100);
            }
        } catch (Exception e) {
            response.put("code",20000);
            response.put("msg","服务忙，请稍后再试");
            log.error("插入数据异常，metadataVo={},异常信息={}", queryVo.toString(),e.getMessage());
        }
        return response;
    }
}
