package com.example.demo.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RefreshScope
@Controller
public class TestController {
	
	@Value("${from}")
	private String from;

	@RequestMapping(value="/from" ,method={RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody String from(){
		return this.from;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}
	
	
}
