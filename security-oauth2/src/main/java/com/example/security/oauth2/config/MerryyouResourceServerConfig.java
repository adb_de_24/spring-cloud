package com.example.security.oauth2.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * 配置资源服务器
 * @author Qiurz
 * @date 2019/12/5 14:54
 */
@Configuration
@EnableResourceServer
public class MerryyouResourceServerConfig {
}
