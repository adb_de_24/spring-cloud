package com.example.security.oauth2.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

/**
 * 认证服务器
 * @author Qiurz
 * @date 2019/12/5 14:54
 */
@Configuration
@EnableAuthorizationServer
public class MerryyouAuthorizationServerConfig {
}
