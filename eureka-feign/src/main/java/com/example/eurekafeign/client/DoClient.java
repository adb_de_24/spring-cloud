package com.example.eurekafeign.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Qiurz
 * @date 2019/11/22 16:23
 */
@FeignClient("eureka-client")
public interface DoClient {

    @RequestMapping("/dc")
    String consumer();
}
