package com.example.eurekafeign.controller;

import com.example.eurekafeign.client.DoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Qiurz
 * @date 2019/11/22 16:26
 */
@RestController
public class DoController {


    @Autowired
    DoClient doClient;

    @RequestMapping("/consumer")
    public String dc(){
        return doClient.consumer();
    }
}
