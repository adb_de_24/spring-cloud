package com.example.sharding.jdbc.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Qiurz
 * @date 2019/11/26 15:19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable,Comparable {

    private static final long serialVersionUID = -1205226416664488559L;
    private Integer id;
    private String username;
    private String password;

    @Override
    public int compareTo(Object o) {
        User u= (User) o;
        return this.id.compareTo(u.id);
    }
}
