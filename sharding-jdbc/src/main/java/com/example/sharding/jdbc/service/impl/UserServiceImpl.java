package com.example.sharding.jdbc.service.impl;

import com.example.sharding.jdbc.entity.User;
import com.example.sharding.jdbc.repository.UserRepository;
import com.example.sharding.jdbc.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Qiurz
 * @date 2019/11/26 15:24
 */
@Service
public class UserServiceImpl  implements UserService {

    @Resource
    UserRepository userRepository;

    @Override
    public Integer addUser(User user) {

        // 强制路由主库
        //HintManager.getInstance().setMasterRouteOnly();
        return userRepository.addUser(user);
    }

    @Override
    public List<User> list() {

        return userRepository.list();
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }
}