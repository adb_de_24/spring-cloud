package com.example.sharding.jdbc.service;

import com.example.sharding.jdbc.entity.User;

import java.util.List;

/**
 * @author Qiurz
 * @date 2019/11/26 15:23
 */
public interface UserService {

    Integer addUser(User user);

    List<User> list();

    void deleteAll();
}