package com.example.sharding.jdbc.repository;

import com.example.sharding.jdbc.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Qiurz
 * @date 2019/11/26 15:21
 */
@Mapper
public interface UserRepository {

    Integer addUser(User user);

    List<User> list();

    void deleteAll();

}