package com.example.redis.service.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Redis缓存工具
 * @author Qiurz
 * @date 2019/12/5 17:04
 */
@Slf4j
@Component
public class RedisUtil {

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     *  判断key对应的value是否存在
     * @param key
     * @return
     */
    public boolean hasExists(final String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     *  写入缓存
     * @param key
     * @param value
     * @return
     */
    public boolean set(final String key,Object value) {
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key,value);
            result = true;
        }catch (Exception e) {
            log.error("写入缓存异常：{}",e);
        }
        return result;
    }

    /**
     *  写入缓存,设置时效
     * @param key
     * @param value
     * @return
     */
    public boolean set(final String key, Object value, Long expireTime, TimeUnit timeUnit) {
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key,value);
            redisTemplate.expire(key,expireTime,timeUnit);
            result = true;
        }catch (Exception e) {
            log.error("写入缓存异常：{}",e);
        }
        return result;
    }

    /**
     *  读缓存
     * @param key
     * @return
     */
    public Object get(final String key) {
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        return operations.get(key);
    }

    /**
     *  删除缓存
     * @param key
     * @return
     */
    public void del(final String key) {
        if (hasExists(key)) {
            redisTemplate.delete(key);
        }
    }

    /**
     *  批量删除缓存
     * @param keys
     * @return
     */
    public void delBatch(final String... keys) {
        for (String key:keys) {
            del(key);
        }
    }

    /**
     *  批量删除key
     * @param pattern
     * @return
     */
    public void delKeyBatch(final String pattern) {
        Set<Serializable> keys = redisTemplate.keys(pattern);
        if (keys.size() > 0){
            redisTemplate.delete(keys);
        }
    }


}
