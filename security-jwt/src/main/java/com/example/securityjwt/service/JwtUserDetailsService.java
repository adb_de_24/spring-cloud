package com.example.securityjwt.service;

import com.example.securityjwt.entity.JwtUser;
import com.example.securityjwt.util.RandomUtil;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author Qiurz
 * @date 2019/11/21 15:41
 */
@Service
public class JwtUserDetailsService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        String pass = new BCryptPasswordEncoder().encode("pass");
        if (!StringUtils.isEmpty(username) && username.contains("user")) {
            return new JwtUser(RandomUtil.randomString(8), username,pass,"USER", true);
        } else {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        }
    }
}
