package com.example.securityjwt.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Qiurz
 * @date 2019/11/21 11:34
 */
@Data
public class JwtResponse implements Serializable {

    private String token;

    public JwtResponse(String token) {
        this.token = token;
    }
}
