package com.example.securityjwt.entity;

import lombok.Data;

import java.io.Serializable;

/**
 *  <p>@Data 注解实现了set和get方法</p>
 * @author Qiurz
 * @date 2019/11/21 11:28
 */
@Data
public class JwtRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;

}
