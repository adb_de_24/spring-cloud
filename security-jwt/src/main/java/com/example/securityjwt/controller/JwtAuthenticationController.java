package com.example.securityjwt.controller;

import com.example.securityjwt.util.JwtTokenUtil;
import com.example.securityjwt.entity.JwtRequest;
import com.example.securityjwt.entity.JwtResponse;
import com.example.securityjwt.entity.JwtUser;
import com.example.securityjwt.service.JwtUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Qiurz
 * @date 2019/11/21 14:34
 */
@RestController
@CrossOrigin
@Slf4j
public class JwtAuthenticationController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/security/auth")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest request) {
        String username = request.getUsername();
        String password = request.getPassword();
        log.info("username={}&password={}",username,password);
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username,password));
        final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @Value("${jwt.header}")
    private String headerToken;

    @GetMapping("/security/token")
    public JwtUser getAuthenticatedUser(HttpServletRequest request) {
        String token = request.getHeader(headerToken).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser jwtUser = (JwtUser) jwtUserDetailsService.loadUserByUsername(username);
        return jwtUser;
    }
}
