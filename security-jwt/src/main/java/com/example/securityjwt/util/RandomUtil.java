package com.example.securityjwt.util;

import java.util.Random;

/**
 * @author Qiurz
 * @date 2019/11/21 15:51
 */
public class RandomUtil {

    public static String randomString(int n) {
        Random random = new Random();
        return String.valueOf(random.nextInt(n));
    }
}
